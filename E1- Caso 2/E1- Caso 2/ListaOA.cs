﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E1__Caso_2
{
    class ListaOA
    {
        public Nodo Cabeza;

        public ListaOA()
        {
            Cabeza = null;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Lista desordenada:");
            Console.WriteLine("");
            Nodo actual = Cabeza;
            while (actual != null)
            {
                Console.WriteLine(actual.valor);
                actual = actual.siguiente;
            }
            Console.WriteLine("");
            Console.WriteLine("Final de la lista desordenada");
            Console.WriteLine("");
        }
        public void AgregarDatos(int Nuevovalor)
        {
            Nodo actual = Cabeza;
            Nodo nuevo = new Nodo();
            nuevo.valor = Nuevovalor;
            while (actual.siguiente != null)
            {
                actual = actual.siguiente;
            }
            actual.siguiente = nuevo;
        }
        public void OrdenarDatos()
        {
            int LugarActual = 1, Total = 1;
            Nodo actual = Cabeza;
            while (actual.siguiente != null)
            {
                actual = actual.siguiente;
                Total++;
            }
            int temporal = 0;
            do
            {
                actual = Cabeza;
                Nodo siguiente = actual.siguiente;
                while (actual.siguiente != null)
                {
                    if (actual.valor > siguiente.valor)
                    {
                        temporal = actual.valor;
                        actual.valor = siguiente.valor;
                        siguiente.valor = temporal;
                        actual = actual.siguiente;
                        siguiente = siguiente.siguiente;
                    }
                    else
                    {
                        actual = actual.siguiente;
                        siguiente = siguiente.siguiente;
                    }
                }
                LugarActual++;
            } while (LugarActual <= Total);
        }
    }
}

