﻿using System;

namespace E1__Caso_1
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaED lista = new ListaED();
            lista.AgregarAlFinal("Hugo", "Huatuco",38);
            lista.AgregarAlFinal("Arturo", "Martinez", 30);
            lista.AgregarAlFinal("Luis", "Campos", 40);
            lista.AgregarAlFinal("Manuel", "Sanchez", 41);
            lista.AgregarAlFinal("Juan", "Perez", 39);
            Console.WriteLine("\nMostrando lista con 5 empleados demo:\n");
            lista.RecorrerLista();
            Console.WriteLine("\nAgregando 2 empleados nuevos:\n");
            lista.AgregarAlFinal("David", "Granados", 31);
            lista.AgregarAlFinal("Mario", "Aquino", 50);
            lista.RecorrerLista();
            Console.WriteLine("\nEliminando 3 empleados de Lista:\n");
            lista.EliminarFinal();
            lista.EliminarFinal();
            lista.EliminarFinal();
            lista.RecorrerLista();

        }
    }
}
