﻿using System;

namespace E1__Caso_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Nodo nodo = new Nodo();
            nodo.valor = 20;
            ListaOA Lista = new ListaOA();
            Lista.Cabeza = nodo;
            Lista.AgregarDatos(2);
            Lista.AgregarDatos(19);
            Lista.AgregarDatos(1);
            Lista.Mostrardatos();
            Console.WriteLine("Lista ordenada en orden ascendente:");
            Console.WriteLine("");
            Lista.OrdenarDatos();
            Lista.Mostrardatos();
            Console.WriteLine();
        }
    }
}
