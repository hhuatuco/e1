﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E1__Caso_1
{
    class ListaED
    {
        public Nodo cabeza;
        public void RecorrerLista()
        {
            Console.WriteLine("\nInicio de la lista\n");
            Nodo actual = cabeza;
            while (actual != null)
            {
                Console.WriteLine(actual.empleado.nombre + " " + actual.empleado.apellido + " " + actual.empleado.edad.ToString());
                actual = actual.siguiente;
            }
            Console.WriteLine("\nFin de la lista\n");
            //Console.WriteLine();
        }
        public void AgregarAlFinal(string nombre, string apellido, int edad )
        {
            Console.WriteLine("Agregando al empleado " + nombre + " " + apellido + " " + edad.ToString());
            Empleado empleado;
            if (cabeza == null)
            {
                cabeza = new Nodo();
                empleado = new Empleado(nombre, apellido, edad);
                cabeza.empleado = empleado;
                cabeza.siguiente = null;
                cabeza.anterior = null;
            }
            else
            {
                Nodo actual = cabeza;
                Nodo nuevo = new Nodo();
                empleado = new Empleado(nombre, apellido, edad);
                nuevo.empleado = empleado;
                while (actual.siguiente != null)
                {
                    actual = actual.siguiente;
                }
                actual.siguiente = nuevo;
                nuevo.anterior = actual;
            }
            Console.WriteLine("");

        }
        public void EliminarFinal()
        {
            Nodo temporal, puntero = new Nodo();
            puntero = cabeza;

            while (puntero.siguiente.siguiente != null)
            {
                puntero = puntero.siguiente;
            }
            temporal = puntero.siguiente;
            Console.WriteLine("Eliminando el empleado: " + temporal.empleado.nombre + " " + temporal.empleado.apellido + " " + temporal.empleado.edad.ToString());
            puntero.siguiente = null;
            temporal = null;
            
        }
    }
}
